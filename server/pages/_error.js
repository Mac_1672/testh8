import React from "react";
import Layout from "../components/Layout";

class Error extends React.Component {

    static getInitialProps ({res,err}) {
        const statusCode = res ? res.statusCode : err ? err.statusCode : null;
        return { statusCode };
    }

    render() {
        return (
            <Layout>
              <div className="container">
                <div className="text-center">
                    <h3>
                        {this.props.statusCode ?
                    'YOU ERROR ' + this.props.statusCode + ' OCCURED ON SERVER' :
                    'An error occured on Client'
                        }
                    </h3>
                </div>
              </div>
            </Layout>
        )
    }

}
export  default Error;
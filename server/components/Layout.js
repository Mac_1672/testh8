import React, { Component} from "react";
import Header from "./Header";
import Footer from "./Footer";
import Head from "next/head";

class Layout extends Component {
    render() {
        const { children, title = "เฮลตี้ คาเฟ่ บล็อกเกอร์"} = this.props;
        return (
            <div>
            <Head>
            <title>{title}</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />

            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/stctic/css/style.css" />

            </Head>
            <Header />
            { children}
            <Footer />
            </div>
        /*ฮะโหล TEST*/
        /*ฮะโหล TEST*/
        /*ฮะโหล TEST*/
        )
    }
}
export default  Layout;

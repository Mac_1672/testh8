import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// import {createStore} from "redux";
// import {createStore } from "redux";
// import {Provider } from "react-redux";

// const store = createStore(reducer, {});

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
